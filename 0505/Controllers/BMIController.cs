﻿using _0505.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace _0505.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            
            var m_height = data.Height / 100;
            data.Result = data.Weight / (m_height * m_height);
            if(ModelState.IsValid)
            {
                
                var level = "";

                if(data.Result <18.5)
                {
                    level = "體重過輕";
                }else if(18.5 <= data.Result && data.Result < 24)
                {
                    level = "正常範圍";
                }else if(24 <= data.Result && data.Result < 27)
                {
                    level = "過重";
                }
                else if (27 <= data.Result && data.Result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= data.Result && data.Result < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= data.Result)
                {
                    level = "重度肥胖";
                }


                
                data.Level = level;

                
            }
            return View(data);
        }
        
    }
}